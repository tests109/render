#!/usr/bin/env bash


# 安装系统依赖
check_dependencies() {
  DEPS_CHECK=("wget" "unzip")
  DEPS_INSTALL=(" wget" " unzip")
  for ((i=0;i<${#DEPS_CHECK[@]};i++)); do [[ ! $(type -p ${DEPS_CHECK[i]}) ]] && DEPS+=${DEPS_INSTALL[i]}; done
  [ -n "$DEPS" ] && { apt-get update >/dev/null 2>&1; apt-get install -y $DEPS >/dev/null 2>&1; }
}

# 修改配置
run() {
unset UUID
for i in 8 4 4 4 12; do UUID=${UUID#-}-$(tr -dc 'a-f0-9' </dev/urandom|head -c $i);done
grep -q UUID config.json && sed -i "s/UUID/$UUID/g" config.json
if [ $? -eq 0 ]; then
HOST=$RENDER_EXTERNAL_HOSTNAME
cat> $RANDOM.html <<EOF

Xray四合一帐号：
帐号：$HOST
密码：$UUID
路径：/vless  /vmess  /trojan  /shadowsocks

VLESS分享链接：
 vless://$UUID@$HOST:443?encryption=none&security=tls&type=ws&host=$HOST&path=/vless#render-vless
-----------------------------------------------------------------------------------------------------------------------------
VMESS分享链接：
 vmess://$(echo -n "{\"v\":\"2\",\"ps\":\"render-vmess\",\"add\":\"$HOST\",\"port\":\"443\",\"id\":\"$UUID\",\"aid\":\"0\",\"net\":\"ws\",\"type\":\"none\",\"host\":\"$HOST\",\"path\":\"/vmess\",\"tls\":\"tls\"}" | base64 -w 0)
------------------------------------------------------------------------------------------------------------------------------
TROJAN分享链接：
  trojan://$UUID@$HOST:443?security=tls&type=ws&host=$HOST&path=/trojan#render-trojan
-----------------------------------------------------------------------------------------------------------------------------
Shadowsocks分享链接：
  ss://$(echo -n "aes-256-gcm://aes-256-gcm:$UUID@$HOST:443:ws:/shadowsocks:$HOST:tls::[]:"|base64 -w0)#
-----------------------------------------------------------------------------------------------------------------------------
EOF

fi
}

check_dependencies
run
